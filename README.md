# MERN App

## Development server

cd mernapp

Run `npm install`

Run `npm start` for a front-end dev server.

cd api

Run `npm install`

Run `nodemon server` for a back-end api server. 

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

https://appdividend.com/2018/11/11/react-crud-example-mern-stack-tutorial/
